# Datenformat für Fragebögen
hier: https://md.kif.rocks/kKiJz-r9SQ6eSxSnqaJSxw?both

# Save
# Datenformat für Fragebögen
Editierbar hier: https://md.kif.rocks/kKiJz-r9SQ6eSxSnqaJSxw?both
## Fragebogenformat
### Questionnaire
Im folgenden gilt die Notation:
- durch js gegebe Typen werden klein geschrieben
- eigene Typen werden groß geschrieben
- `Ausdruck : Typ`    (der linke Ausdruck hat den rechts angegebenen Typs)
- `Ausdruck : [Typ]`    (der linke Ausdruck besteht aus einem Array des rechten Typs)
- `Ausdruck : { ... }`    (der linke Typ besteht aus einem Objekt, wobei ... eine Liste von Variablen mit Typen ist)
- `(Ausdruck)`    (der Ausdruck ist optional)
- `Ausdruck : Typ1 | Typ2`    (der Ausdruck hat entweder Typ1 oder Typ2)

```javascript
Questionnaire: { 
    "id": string, 
    "welcomePage": string, 
    "gdprNotice": string, 
    "questionset": Questionset, 
    "endPage": string,  
    "starttime" : number, 
    "endtime": number
}
Questionset : [Question]
Question : { "id" : number, ("title" : string), "questiontext" : string, "answers" : [Answeroption] }
Answeroption : { "id" : number, "type" : AnswerType, ("task" : string), (...) } | string
AnswerType : "checkbox" | "radiobutton" | "dropdown" | "freetext" | "compose" | "checkbox-matrix" | "radiobutton-matrix"
```
Folgende zusätzliche Felder werden für Answeroption in Abhängigkeit vom Wert type erwartet:
```yml
"freetext"
- (defaultText : string)
- (rows : number)
- cols : number
"dropdown"
- options : [string]
"radiobutton"
- options : [Answeroption]
"checkbox"
- options : [Answeroption]
"compose"
- options : [Answeroption]
"checkbox-matrix"
- labels : [string]
- options : [string]
"radiobutton-matrix"
- labels : [string]
- options : [string]

```
### Answers
```javascript
Answers: { "qid": string, "uid": string,  "answerset": [Answer] }
Answer : { "answerid": number, "value": string }
```
## Beispiele
### Questionnaire
```javascript
{
  "id": "7f4hDJ9HNnQ0LfZcseoYnRZX9snbjk",
  "name": "test survey",
  "starttime": 1528358766,
  "endtime": 1530000000,
  "questionset": [
    {
      "id": 0,
      "questiontext": "What ist your gender?",
      "answers": [
        {
          "id": 0,
          "type": "radiobutton",
          "text": "Select one option.",
          options: [
            "female",
            "male",
            {
              "id": 1,
              "type": "freetext",
              "cols":1,
              "rows:"25
            }
          ]
        },
        {
          "id": 1,
          "questiontext": "How old are you?",
          "answers": [
            {
              "id": 2,
              "type": "freetext",
              "text": "Please insert your age below.",
              "cols": 1
            }
          ]
        }
      ]
    }
  ]
}
```
### Answers
```javascript
{
  "qid": "7f4hDJ9HNnQ0LfZcseoYnRZX9snbjk",
  "uid": "NOy2OA2DA7zKPVcKgv3RelpNeXVqBy",
  "answerset": [
    {
      "answerid": 0,
      "value": 0
    },
    {
      "answerid": 1,
      "value": "sächlich"
    },
    {
      "answerid": 2,
      "value": "82"
    }
  ]
}


```


