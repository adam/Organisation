`moved to wiki`
# Blöcke der Umsetzung:
### Einigung auf eingesetzte Werkzeuge
Unter anderem:
- Frameworks, Sprachen
- Testing tools
- Code Quality Assurance Tools (Linter, usw.)
- IDE

### Spezifikation eines Fragebogenformats
Beispielsweise gehört dazu:
- leichte Darstellbarkeit im Browser
- Fragebögen haben mehrere Seiten, die auch im Format getrennt sein müssen
- ausbaubar in Bezug auf Formatierungsoptionen
- gut maschinell verarbeitbar (alle Antwortmöglichkeiten müssen extrahiert werden)
- testbar


### Aufsetzen einer Entwicklungumgebung
Beispielsweise könnte dazu gehören:
- gitlab repo
- Continuous integration
 - docker image bauen
- aufsetzen der frameworks und tools (z.B. meteor, vue, webpack, eslint, ...)
- IDE integration sicherstellen
- wiki seite schreiben, wie man sich die Entwicklungsumgebung am besten lokal einrichtet

## Aufteilung in Server und Client (bzw. Front und Backend)
Frontend:
- Fragebogenrender
- Basic Frame (GUI Darstellung)
- Funktionalität

Backend:
- Design und Anbindung Datenbank
- Auslieferung der Fragebögen an den Client
- Schnittstellen Design für Client


### GUI Design Implementation
- Teil des Frontends
- Gestaltung und Implementation der GUI anhand der Wireframes

### Design Client Funktionality, Implementation davon
- Einbinden des Renders zur Fragendarstellung (Schnittstelle konzeptionieren)
- Kommunikation mit der Server Schnitstelle.
- Steuerung der Userinteraktion.
- Abgreifen der Antworten, die der Nutzer eingibt und übertragung an den Server.

### Renderer für das Fragebogenformat entwerfen, schreiben
Implementation der Schnittstelle
Das sollte etwa so aussehen:
- der fragebogen liegt vor
- der fragebogen aufgeteilt in verschiedene Ansichten (Seiten)
- die verschiedenen Seiten werden dann aus dem format in html übersetzt
- dabei werden die vom Nutzer einzugebenen Werte in geeigneter Form an die Datenbank gekoppelt

### Datenbankdesign
ergibt weitestgehend ziemlich aus dem Fragebogenformat. Dazu gehört die Repräsentation von 
- Nutzern, Fragebögen, Antworten zu Fragebögen

### Server Design, Implementation davon
stark verschränkt mit Datenbankdesign, sowie Rendering.  
Dazu gehören hier beispielsweise:
- stumpfe tokenbasierte Authentifizierung (aka Zugriff über Link)
- Anbindung der Datenbank an den Client  ($### Sollte die Datenbank nicht an den Server angebunden werden und nicht an den Client? 

### Spezifikation der Schnittstelle
je nach Framework muss die Schnittstelle genau spezifiziert werden oder es sollte aufgeschrieben werden, 
welche Daten an welcher Stelle anfallen und wie und wann die zwischen Server und Client automatisch ausgetauscht werden.


### Testphase
Ein Teil der Testphase findet dauerhaft in Form von Testdriven Developement statt. Es werden extra Testphasen benötigt für (Reihenfolge):
- Erstellte Spezifikation udn Interfaces müssen gereviewt und die Logik geprüft werden 
- GUI Usability Testing (frühzeitig)
- Integration Tests (Test der Kommunikation zwischen Client und Server)
- Stress und Belastungtest (sind die geforderten gleichzeitigen Zugriffe möglich, was passiert bei mehr)
- Feldtest (Durchführung einer Umfrage)
- Spezielle Tests für die Forschungsfrage


### GUI Implementation
Teilphase der Implementation. Die aktuellen Wireframes müssen weiter evaluiert und angepasst werden.
Erste prototypische Implementation mit minimaler Funktion sollte frühzeitig fertiggestellt werden.
Für den Prototypen sollten erneut Usability Tests durchgeführt werden, damit Feedback und Probleme rechtzeitig erkannt und behoben werden können.

## Abhängigkeiten:


<!--Anmerkung: Die Phasen müssen nicht abgeschlossen sein um in die nächste Eintreten zu können.-->
Die Phasen sollten jeweils abgeschlossen sein, um in die nächste eintreten zu können.

|erst|dann|
| ----- | ----- |
|Festlegung der eingesetzte Werkzeuge|Entwicklungsumgebung, Spezifikation der Schnittstelle, ..., Datenbankdesign|
|Fragebogenformat|Server Design, Datenbankdesign|
|Spezifikation der Schnittstelle|Server Design, Client Design, Renderer|
|Konzeption der einzelnen Teilmodule| Review dieser|
|Review einzelner Teilmodule|Implementation dieser|
|GUI Implementation|Erneute Usability Tests|
|Implementationsphasen| Testphasen|
|Integrationstest|benötigte Teilmodule implementiert und getestet|



Deshalb vorgeschlagen:
1. eigesetzte Werkzeuge
2. Fragebogenformat
3. Review Fragebogenformat, Entwicklungsumgebung
4. Spezifikation der Schnittstelle, Design Client Funktionalität, Server Design
5. Beginn Implementation GUI, Review der Schnittstelle & Server Design & CLient Funktionalität
6. Konzeption Renderer & Datenbank, Implementation Schnittstelle, Server & Client Funktionalität
7. Beginn Testphase GUI, Review der Konzepte
8. Implementation Renderer & Datenbank
9. Abschließende Testphasen jeweils für CLient und Server
10. Integrationstest (speziell Kommunikation Client-Server und Server-Datenbank)
11. Stress und Belastungstest
12. Feldtest
13. extra Test für Fragestellung





