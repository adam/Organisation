### Interview questions
Link zum Pad: https://pad.informatik.uni-goettingen.de/p/2018-04-18-PP-InterviewKlingebiel

1 - Anforderungen: 

    * Welche Anforderungen stellt die Uni an ein Onlineumfragesystem? !!!!!!!!!!!!!!!!!
    * Welche Funktionalitäten muss ein Onlineumfragesystem erfüllen? 
    * Welche Herausforderungen sehen Sie bei einem Onlineumfragesystem, dass an Universitäten genutzt wird? (Warum?)
    * An welchen Stellen muss bei der Veranstaltungsevaluation besonders viel manuell gearbeitet werden? 
    * Welche Zugriffsbeschränkungen unterschiedlicher Rollen sind in Evasys implementiert?
    * Gibt es weitere Zugriffskontrollmechanismen, die ihrer Meinung nach notwendig wären? 
    * Sind in EvaSys für alle Rollen die Daten vollständig anonymisiert?
    * Welche Funktionalitäten sind auf welcher Art von Endgeräten unbedingt erforderlich?
2 - Nutzergruppen:

    * Welche Personengruppen nutzen EvaSys?
    * Gibt es auffällige Häufungen von Beschwerden bestimmter Gruppen zu EvaSys?
    * Welches ist die Hauptnutzergruppe? 
3 - Datenschutz:

    * Welche Datenschutzrichtlinien muss die Uni nach außen erfüllen?
    * Gibt es interne Datenschutzrichtlinien, die sich die Uni selber auferlegt?
4 - Diverses:

    * Welche Richtlinien gibt es bezüglich Einbindung von StudIP-Plugins? -> haben "Fragebogen" gesehen, ist es Feature oder Plugin von StudIP
    * Gibt es weitere Personen, von denen Sie denken, dass wir mit diesen sprechen sollten? Wenn ja, wer?
    * Inwiefern 


# Mitschrift
## Anforderungen der Uni an EvaSys
- (sehr) gute Filterführung
- individuelle Befragungen
- Datenschutz (Questback Premium)
- belastbar
- templates
- für jeden benutzbar
- Einscannen von Fragebögen / Zuordnung zu Fragebogen
- Responsive Design, mobil
- Flexibilität
- schnelle Benutzung
- schnelle Bearbeitung
- Mehrsprachigkeit
- nach Fakultät filtern, bedingt Fragen einschieben
- Agilität vs. Statik der Fragebogenerstellung
- Papierfragebögen
- Encoding, Umlaute
- Kompatibilität
- Sprachen umschalten
- Beantwortung unterbrechen
- Zurückbutton (Ja/Nein/Konfigurierbar)
- Fortschrittsanzeige (intelligente Fortschrittsberechnung)
- Freitextfelder

## Statistik
- wer hat mitgemacht?
- wann?
- wieviele zu welcher Zeit?
- auf welchem Gerät?

## Besondere Herausforderungen
- StudIP Schnittstelle
- XML Metadaten von univz
- Fragebögen müssen Studiengang/Fakultät zugeordnet sein

## Was fehlt
- Onlineevaluation
- Massenoperationen (mehrere Bögen abschicken)
- Zeitschaltung
- Templates
- Einsicht: Dropdown

## Nutzergruppen
- Unipark
- Zentrale Evaluation
- Fakultäten
- meist für Lehrevaluation genutzt

## Rechtekonzept
- Berechtigungsstufen (differenzierte Leserechte)


- so wenig wie möglich fragen
- Nutzerschulung, intuitive Benutzung, technische Illiteraten, gute Dokumentation
- auf einer Seite, bloß nicht scrollen

- WLAN Ausnutzung 
- Studiendekanat ins Boot holen
- Anonymisierung vs. Daten isoliert, ausgeblendet weiterhin behalten
- Freitextfelder Datenschutz (Name angegeben)
- Freitextfelder Beleidigungen filtern (gesetzliche Verpflichtung)
- Opt-Out
- Datenschutzbelehrung, aktive Zustimmung
- im Nachgang Widerspruch Einlegen, Daten löschen lassen

## Datenschutzrichtlinien
- Zweck muss Qualitätssicherung sein
- An Datenschutzbeauftragten wenden
- Datenschutzbeauftragter muss Antrag prüfen
- Verfahrensbeschreibung
- Datenschutzgesetz
- Uniinterne Datenschutzrichtlinie

# Weitere Fragen
- StudIP Plugin Richtlinien (Antwort: nein, vielleicht doch technische?)

## Personen ansprechen
- Datenschutzbeauftrager
- Herr Winkler (univz)
- Nutzerkreis Fragebogenerstellung
- Methodenzentrum SoWi (Fragebogenerstellung)
- Digital Humanities


## Ergänzung unsortiert:
    * Person Datenschutzbeauftragter der Uni Laschke, hat auch  -> kennt Richtlinien
    * Verfahrensbeschreibung runterladen
    * Auftragsdatenverarbeitung - wenn Server extern
    * ISO9001
    * Datenschutzverordnung Bund
    * Online UniDatenschutzverordnung
    * nocheinmal mit Anke Klingebiel reden
    * studIP-plugin ist nur EvaSys Anbindung
    * Beleidigungen müssen gelöscht werden
    * Nutzbarkeit von EvaSys (StudiumLehre, Dozenten, Studierende)
    * Dokumentation/Handbuch - Vorlage
    * QM Handbuch -Download
    * Papierversion vorhalten - feature    
    * Winkler - UniVZ Anmbidnung
    * Herr Leihbold - Methodenzentrum/SoWi
    * Questback Handbuch (hat Fragetypen), LimeSurvey auch
    * Codierung
    * Browserkompatibilität
    * Reporting - Feature
    * immer mehr Mobil
    * features ... recht individuell je Fragebogen 
    * Frau Sporleder DigiMon
    
    
